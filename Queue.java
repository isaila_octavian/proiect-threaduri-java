package maven;
import java.util.ArrayList;




public class Queue implements Runnable{
	private ArrayList<Client> clienti = new ArrayList<Client>();
	public Queue(int clients)
	{
			clienti=new ArrayList<Client>();
			clienti.add(new Client(0,1));
	
	}
	public int getSuma(){
	    int sumaServiceTime = 0;
		if(clienti.isEmpty()) return 0;
		else {
			for(Client a : clienti) {
				sumaServiceTime = sumaServiceTime + a.getSimulationTime();
									}
				return sumaServiceTime;
			}
		}
	public String toString()
	{
		String clients="[";
	for (int i=0;i<clienti.size();i++)
	{
		clients=clients+clienti.get(i).toString() +" | ";
	}
	clients+="]";
	return clients;
	}
	public void addClient(Client newClient) {
			clienti.add(newClient);
	}
	public void deleteClient(){
		if(clienti.size()>0)
			clienti.remove(0);
	}
	public int getSize() {
		return clienti.size();
	}
	public void run(){
		Client newClient=clienti.get(0);
		while(newClient.getSimulationTime()>0) {
			newClient.setSimulationTime(newClient.getSimulationTime()-1);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (newClient.getSimulationTime()==0) {
				synchronized(GUI.textArea)
				{
					GUI.textArea.append("Clientul "+(newClient.getArrivalTime())+  " a plecat de la magazin la  "+ (SimulationManager.getCurrent()-1)+"\n"); 
					GUI.textArea.setCaretPosition(GUI.textArea.getDocument().getLength());
				}
				clienti.remove(0);
			if (clienti.size()==0){
				while( clienti.size()==0){
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}}
				newClient=clienti.get(0);
				}
			else {
				newClient=clienti.get(0);}}}
	}}

