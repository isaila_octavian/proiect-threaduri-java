package maven;


import java.io.IOException;
import java.util.ArrayList;



public class SimulationManager implements Runnable {
	public  int timeLimit;
	public int maxSimulationTime;
	public int minSimulationTime;
	public int maxArrivalTime;
	public int minArrivalTime;
	public int numberOfQueues;
	public int numberOfClients;
	private int totalTime=0;

	public static SelectionPolicy selectionPolicy;
	
	private static Scheduler scheduler;
	private static int currentTime;
	private ArrayList<Client> generatedClients=new ArrayList<Client>();
	
	public SimulationManager(int numberClients,int minArrivalTime, int maxArrivalTime, int minProcessingTime, int maxProcessingTime,int numberOfQueues,int timeLimit)
	{
	this.timeLimit=timeLimit;
	this.minArrivalTime=minArrivalTime;
	this.maxArrivalTime=maxArrivalTime;
	this.minSimulationTime=minProcessingTime;
	this.maxSimulationTime=maxProcessingTime;
	this.numberOfQueues=numberOfQueues;
	this.numberOfClients=numberClients;
	scheduler=new Scheduler(numberOfQueues,numberOfClients);
	scheduler.changeStrategy(selectionPolicy);
	generateRandom(numberClients,maxArrivalTime,minArrivalTime,maxProcessingTime, minProcessingTime);
	}
	public void generateRandom(int numberOfClients,int arrivalMAX,int arrivalMin,int serviceMAX, int serviceMin) {
		int randomArrivalTime=0;
		while(numberOfClients > 0) {
			try{	
				Client c;
				int randomSimulationTime=(int) ((Math.random()*(maxSimulationTime-minSimulationTime+1))+minSimulationTime);
				randomArrivalTime=(int)((Math.random()*(maxArrivalTime-minArrivalTime+1)));
				c=new Client(randomArrivalTime,randomSimulationTime);
				generatedClients.add(c);
				numberOfClients--;//decrementare
			}catch(Exception E) {}
			}
		
	}
	public synchronized static int getID()
	{
		return (scheduler.getID()+1);
	}
	public synchronized static int getCurrent()
	{
		return (currentTime);
	}
	public int getAvgTime() {
		return totalTime/numberOfClients;
	}
	public void run() {
		currentTime=0;
		while(currentTime<timeLimit) {
			System.out.println(currentTime);
			for(int i=0;i<numberOfClients;i++) {
				if (generatedClients.get(i).getArrivalTime()==currentTime) {
					scheduler.dispatchClient(generatedClients.get(i));
					totalTime+=generatedClients.get(i).getSimulationTime();
					if(generatedClients.get(i).getArrivalTime()!=0) {
						synchronized(GUI.textArea)
						{
							GUI.textArea.append("Clientul " + (generatedClients.get(i)) +" ajunge la momentul "
									 +generatedClients.get(i).getArrivalTime() +" la coada " + (scheduler.idCoada+1) +"\n"); 
							GUI.textArea.setCaretPosition(GUI.textArea.getDocument().getLength());
						}}}}
			String s="";
	 for (Queue q: scheduler.queues) {
				s=s+q.toString()+"\n";}
	 GUI.textArea2.setText(s);
	 GUI.textArea2.revalidate();
			currentTime++;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();}
			}
		GUI.textArea2.setText("Average waiting time: "+getAvgTime());
}
	 public static void main( String[] args )
	    {
		GUI g=new GUI();
	    }
	
}